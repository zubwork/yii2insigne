<?php

namespace backend\controllers;

use common\models\User;
use yii\db\Query;
use yii\filters\auth\HttpBasicAuth;
use yii\rest\ActiveController;
use Yii;
use yii\web\MethodNotAllowedHttpException;
use yii\widgets\ActiveForm;

class RestController extends ActiveController
{
    const LOGIN = 'rest';
    const PASSWORD = 'rest';

    public $modelClass = 'common\models\User';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            'auth' => [$this, 'auth']
        ];

        return $behaviors;
    }

    protected function verbs()
    {
        return [
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH', 'POST'],
            'delete' => ['DELETE'],
            'view' => ['GET'],
            'index' => ['GET'],
        ];
    }

    public function Auth($username, $password)
    {
        if ($username == self::LOGIN && $password == self::PASSWORD) {

            $model = new User();
            return $model;
        }
        return null;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        unset($actions['create']);
        unset($actions['delete']);
        unset($actions['update']);
        unset($actions['view']);
        return $actions;
    }

    public function actionIndex()
    {
        $users = (new Query())
            ->from('user')
            ->select(
                [
                    'user.id',
                    'username',
                    "CONCAT(name, ' ', surname, ' ', patronymic) AS full_name",
                    "DATE_FORMAT(FROM_UNIXTIME(date_end, '%Y-%m-%d'), '%d-%m-%Y') AS Date"
                ])
            ->leftJoin(['s' => 'user_subscription'], 'user.id = s.user_id')
            ->all();

        if (!empty($users)) {
            return json_encode(['data' => $users], JSON_BIGINT_AS_STRING);
        } else {
            throw new MethodNotAllowedHttpException('You are not allowed to update data');
        }
    }

    public function actionUpdate($id)
    {
        $params = Yii::$app->request->post();

        if (!empty($params)) {
            $model = new User();

            $modelUser = $model::find()
                ->where(['id' => $id])
                ->limit(1)
                ->one();

            if(!empty($params['new_password'])){
                $modelUser->updatePassword($params['new_password']);
            }
            //Todo: проверить аттрибуты
            $modelUser->attributes = $params;

            if ($modelUser->validate() ) {
                $modelUser->save();
                return 'Данные обновились';
            } else {
                return (ActiveForm::validate($modelUser));
            }
        } else {
            throw new MethodNotAllowedHttpException('You are not allowed to update data');
        }
    }
}