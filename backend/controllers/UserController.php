<?php

namespace backend\controllers;

use backend\models\HandlerSubscription;
use backend\models\UserSearch;
use Yii;
use common\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new UserSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $modelUser = $this->findModel($id);

        $dataUser = Yii::$app->request->post('User');
        $dataEndSubscription = Yii::$app->request->post('UserSubscription');

        if(!empty($dataUser['new_password'])){
            $modelUser->updatePassword($dataUser['new_password']);
        }

        if ($modelUser->load(Yii::$app->request->post()) && $modelUser->save()) {

            if (!empty($modelUser->userSubscription) || $dataEndSubscription) {
                $handlerSubscription = new HandlerSubscription($id);
                $handlerSubscription->handlerUserSubscription();
            }

            return $this->redirect(['view', 'id' => $modelUser->id]);

        } else {
            return $this->render('update', [
                'model' => $modelUser,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
