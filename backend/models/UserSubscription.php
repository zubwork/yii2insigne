<?php

namespace backend\models;

use common\models\User;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_subscription".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $date_end
 *
 * @property User $user
 */
class UserSubscription extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'user_subscription';
    }

    public function rules()
    {
        return [
            ['user_id', 'unique'],
            ['user_id', 'integer'],
            ['date_end', 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'date_end' => 'Date End Subscribe',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function findModel($user_id)
    {
        if (($model = self::find()->where(['user_id' => $user_id])->limit(1)->one()) !== null) {
            return $model;
        } else {
            return false;
        }
    }

}
