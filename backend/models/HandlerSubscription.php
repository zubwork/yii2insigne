<?php

namespace backend\models;

use Yii;

class HandlerSubscription
{
    private $userSubscription;
    private $user_id;


    public function __construct($user_id)
    {
        $this->userSubscription = new UserSubscription();
        $this->user_id = (int)$user_id;
    }


    public function handlerUserSubscription()
    {
        $userIsSubscription = $this->userSubscription->findModel($this->user_id);

        if (!empty($userIsSubscription)) {

            if ($userIsSubscription->load(Yii::$app->request->post())){
                if ($userIsSubscription->getAttribute('date_end')) {
                    $userIsSubscription->setAttribute('date_end', strtotime($userIsSubscription->getAttribute('date_end')));
                    $userIsSubscription->save();
                } else {
                    $userIsSubscription->delete();
                }
            }

        } else {

            if ($this->userSubscription->load(Yii::$app->request->post())) {
                $this->userSubscription->setAttribute('date_end', strtotime($this->userSubscription->getAttribute('date_end')));
                $this->userSubscription->user_id = $this->user_id;
                $this->userSubscription->save();
            }
        }
    }
}