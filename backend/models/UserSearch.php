<?php

namespace backend\models;

use common\models\User;
use yii\data\ActiveDataProvider;


class UserSearch extends User
{

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['username', 'email'], 'safe'],
            [['fullName'], 'safe']
        ];
    }

    public function attributes()
    {
        return  array_merge(parent::attributes(), ['fullName']);
    }

    public function search($params)
    {
        $query = User::find()->with('userSubscription');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'username',
                'email',
                'fullName' => [
                    'asc' => ['name' => SORT_ASC, 'surname' => SORT_ASC, 'patronymic' => SORT_ASC],
                    'desc' => ['name' => SORT_DESC, 'surname' => SORT_DESC, 'patronymic' => SORT_DESC],
                    'label' => 'Full Name',
                    'default' => SORT_ASC
                ],
            ]
        ]);


        if (!$this->load($params) && $this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id
        ]);

        $query->andFilterWhere(['LIKE', 'username', $this->username])
            ->andFilterWhere(['LIKE', 'email', $this->email]);
        $query->orFilterWhere(['LIKE', 'name', $this->getAttribute('fullName')])
            ->orFilterWhere(['LIKE', 'surname', $this->getAttribute('fullName')])
            ->orFilterWhere(['LIKE', 'patronymic', $this->getAttribute('fullName')])
            ;

        return $dataProvider;
    }

}
