<?php

namespace console\controllers;

use yii\console\Controller;
use Yii;

class SubremController extends Controller
{
    public function actionRemove()
    {
        $dateNowUnix = strtotime('today');

        $count = Yii::$app->db->createCommand()->delete('user_subscription', 'date_end <'.$dateNowUnix)->execute();
        echo $count . " deleted subscriptions \n";
    }
}