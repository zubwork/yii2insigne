<?php

use yii\db\Migration;

/**
 * Handles adding name_column_surname_column_patronymic to table `user`.
 */
class m170303_180752_add_name_column_surname_column_patronymic_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'name', $this->string()->after('email'));
        $this->addColumn('user', 'surname', $this->string()->after('name'));
        $this->addColumn('user', 'patronymic', $this->string()->after('surname'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'name');
        $this->dropColumn('user', 'surname');
        $this->dropColumn('user', 'patronymic');
    }
}
