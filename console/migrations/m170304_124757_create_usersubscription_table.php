<?php

use yii\db\Migration;

/**
 * Handles the creation of table `usersubscription`.
 */
class m170304_124757_create_usersubscription_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('user_subscription', [
            'id' => $this->primaryKey(),
            'user_id' => 'int',
            'date_end' => 'int'
        ]);

        $this->addForeignKey('user_user_subscription_id', 'user_subscription', 'user_id', 'user', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('user_subscription');
    }
}
